#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

char *sLogfile1, *sLogfile2;
int iLogInterval1, iLogInterval2 ;
FILE *fd1, *fd2;
pthread_t thread_id1, thread_id2;

void* thread1(void* arg)
{ 
    time_t current_time, write_time;
    struct tm * time_info;
    char timeString[22];

    current_time = time(NULL);
    write_time = current_time + iLogInterval1;
    for(;;)
    {
        current_time = time(NULL);
        if(current_time >= write_time){

            fd1 = fopen(sLogfile1, "a");

            if (fd1 != NULL)
            {
                printf("Save current time to file: %s\r\n", sLogfile1);
                //time(&current_time);
                time_info = localtime(&current_time);

                strftime(timeString, sizeof(timeString), "%d/%m/%Y %H:%M:%S\n", time_info);
                fputs(timeString, fd1);
                fflush(fd1); 
            }
            else
            {
                printf("Error while opening file... %s\n\r", sLogfile1);
            }
            fclose(fd1);

            write_time = time(NULL) + iLogInterval1;

        }
    }
    return NULL; 
}
void* thread2(void* arg)
{ 
    time_t current_time, write_time;
    struct tm * time_info;
    char timeString[22];

    current_time = time(NULL);
    write_time = current_time + iLogInterval2;
    for(;;)
    {
        current_time = time(NULL);
        if(current_time >= write_time){

            fd2 = fopen(sLogfile2, "a");

            if (fd2 != NULL)
            {
                printf("Save current time to file: %s\r\n", sLogfile2);
                //time(&current_time);
                time_info = localtime(&current_time);

                strftime(timeString, sizeof(timeString), "%d/%m/%Y %H:%M:%S\n", time_info);
                fputs(timeString, fd2);
                fflush(fd2);
            
            }
            else
            {
                printf("Error while opening file... %s\n\r", sLogfile2);
            }
            fclose(fd2);

            write_time = time(NULL) + iLogInterval2;
        }
    }
    return NULL; 
}

int main(int argc, char *argv[])
{
    // bij 5 argumenten: dus programma-naam, bestandsnaam 1, loginterval 1, bestandsnaam 2, loginterval 2
    if (argc == 5)
    {
        sLogfile1 = argv[1];
        sscanf(argv[2], "%i", &iLogInterval1);
        sLogfile2 = argv[3];
        sscanf(argv[4], "%i", &iLogInterval2);

        if (pthread_create(&thread_id1, NULL, &thread1, NULL) != 0) 
            printf("\nThread 1 can't be created."); 
        if (pthread_create(&thread_id2, NULL, &thread2, NULL) != 0) 
            printf("\nThread 2 can't be created."); 
  
        pthread_join(thread_id1, NULL); 
        pthread_join(thread_id2, NULL); 
    }
    else
    {
        printf("Error: command does not contain the correct arguments..\r\n");
    }

    return 0;
}
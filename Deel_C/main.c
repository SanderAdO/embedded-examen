#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

char *sLogfile;
int i_N_Threads; // aantal threads

pthread_t *threads = NULL;
pthread_mutex_t lock;

typedef struct
{
    int iThreadNum;
    int iLogInterval;
} thread_arg_struct;

void *thread_handler(void *args)
{
    thread_arg_struct *thread_struct = args;
    time_t current_time, write_time;
    struct tm *time_info;
    char timeString[22];
    char threadString[15];
    int iThread;
    FILE *fd;

    current_time = time(NULL);
    write_time = current_time + thread_struct->iLogInterval;

    for (;;)
    {
        current_time = time(NULL);
        if(current_time >= write_time){

            pthread_mutex_lock(&lock);  // lock de mutex
            fd = fopen(sLogfile, "a");

            if (fd != NULL)
            {
                printf("Thread number %d saved time to logfile: %s\r\n", thread_struct->iThreadNum, sLogfile);
                //time(&current_time);
                time_info = localtime(&current_time);
                strftime(timeString, sizeof(timeString), "%d/%m/%Y %H:%M:%S\n\r", time_info);
                snprintf(threadString, 15, "Thread %d, ",thread_struct->iThreadNum);
                fputs(threadString, fd);
                fputs(timeString, fd);
                fflush(fd);
            }
            else
            {
                printf("Error while opening file... %s\n\r", sLogfile);
            }
            fclose(fd);
            pthread_mutex_unlock(&lock);    // unlock de mutex

            write_time = time(NULL) + thread_struct->iLogInterval;
            //sleep(thread_struct->iLogInterval);
        }
    }
    free(thread_struct);
    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc > 2)
    {
        int x;
        sLogfile = argv[1];
        printf("Write to file: %s\r\n", sLogfile);
        i_N_Threads = argc - 2;
        printf("Create %d threads..\r\n", i_N_Threads);
        threads = malloc(sizeof(pthread_t)*i_N_Threads);

        for (x = 0; x < i_N_Threads; x++)
        {   
            int iLogInterval;
            thread_arg_struct *args = malloc(sizeof *args);
            if(args == NULL)
                printf("Error: no args given...\n\r");
            if(sscanf(argv[x + 2], "%i", &iLogInterval) != 1)
            {
                printf("Interval for thread %d is not an integer\r\n", x+1);
            }
            args->iThreadNum = x+1;
            args->iLogInterval = iLogInterval;
            if(pthread_create(&threads[x], NULL, &thread_handler, args)) {
                free(args);
                printf("Error while creating thread... %d\r\n", x);
                return 1;
            }
            printf("Thread %d created with interval of %d seconds\r\n", x+1, iLogInterval);
        }

        if (pthread_mutex_init(&lock, NULL) != 0)
        {
            printf("\n mutex init has failed\n");
            return 1;
        }
        
        for (x = 0; x < i_N_Threads; x++)
        {
            pthread_join(threads[x], NULL); 
        }
        pthread_mutex_destroy(&lock);
    }
    else
    {
        printf("Error: command does not contain the correct arguments..\r\n");
    }

    return 0;
}
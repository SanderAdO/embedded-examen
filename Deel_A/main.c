#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char *argv[])
{
    char* sLogfile;
    int iLogInterval;
    time_t current_time, write_time;
    struct tm * time_info;
    char timeString[22];  //22 want: "dd-mm-YYYY HH:MM:SS\0"
    FILE *fd;

    // 3 argumenten: programma-naam, bestandsnaam en loginterval
    if (argc == 3)
    {
        sLogfile = argv[1];
        sscanf(argv[2], "%i", &iLogInterval);
        fd = fopen(sLogfile, "a");

        if (fd != NULL)
        {
            current_time = time(NULL);
            write_time = current_time + iLogInterval;
            for (;;)
            {
                current_time = time(NULL);
                if(current_time >= write_time){

                    printf("Save current time to file: %s\r\n", argv[1]);
                    //time(&current_time);
                    time_info = localtime(&current_time);

                    strftime(timeString, sizeof(timeString), "%d/%m/%Y %H:%M:%S\n", time_info);
                    fputs(timeString, fd);
                    fflush(fd);
                    write_time = (time(NULL) + iLogInterval);
                }
            }
            
        }
        else
        {
            printf("Error while opening file... %s\n\r", sLogfile);
        }
        fclose(fd);
    }
    else
    {
        printf("Error: command does not contain the correct arguments..\r\n");
    }

    return 0;
}